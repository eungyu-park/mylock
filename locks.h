#ifndef __LOCKS_H__
#define __LOCKS_H__

#include <pthread.h>
enum lock_types;

/**
 * Have a look at https://linux.die.net/man/3/list_head for using list_head
 */
#include <sys/queue.h>
struct thread {
	pthread_t pthread;
	unsigned long flags;
	TAILQ_ENTRY(thread) next;
};

/*************************************************
 * Spinlock
 */
struct spinlock {
	volatile int integer;
};
void init_spinlock(struct spinlock *);
void acquire_spinlock(struct spinlock *);
void release_spinlock(struct spinlock *);


/*************************************************
 * Mutex
 */
struct mutex {
	volatile int integer;
};
void init_mutex(struct mutex *);
void acquire_mutex(struct mutex *);
void release_mutex(struct mutex *);


/*************************************************
 * Semaphore
 */
struct semaphore {
	volatile int integer;
	volatile int kind;	//0:full, 1:mutex, 2:empty
};
void init_semaphore(struct semaphore *, const int);
void wait_semaphore(struct semaphore *);
void signal_semaphore(struct semaphore *);


/*************************************************
 * Lock tester.
 * Will be invoked if the program is run with -T
 */
void test_lock(void);


/*************************************************
 * My Waiting List implementation.
 */
void init_list();
struct listEntry *insert_list(pthread_t tid);
struct listEntry *get_listElemPtr_mutex();
struct listEntry *get_listElemPtr_semaphore(int sem_kind);
void remove_list(struct listEntry *tar);
void clean_all_list();


/*************************************************
 * My signal handler implementation.
 */
void signal_handler(int sig);


#endif
