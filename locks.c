#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/queue.h>
#include <string.h>
#include <signal.h>

#include "config.h"
#include "locks.h"
#include "atomic.h"


/******************************************************************
 * My Waiting List implementation.
 * List-related tasks.
 * 
 * Definition, Initialization, Insert, Remove(pop), Clean(free).
 */
LIST_HEAD(listHead, listEntry) listhead;
struct listEntry {
	volatile pthread_t tid;
	volatile int chkrcv;
	volatile int kind;					//0:full, 1:mutex, 2:empty
	LIST_ENTRY(listEntry) listEntries;
};

void init_list(){
	LIST_INIT(&listhead);
}

struct listEntry *insert_list(pthread_t tid){
	struct listEntry *new = (struct listEntry *)malloc(sizeof(struct listEntry));
	new->tid = tid;
	new->chkrcv = (volatile int)0;
	struct listEntry *tar = LIST_FIRST(&listhead);
	if (tar == NULL){
		LIST_INSERT_HEAD(&listhead, new, listEntries);
		return new;
	}
	else{
		while(tar->listEntries.le_next != NULL){
			tar = tar->listEntries.le_next;
		}
		LIST_INSERT_AFTER(tar, new, listEntries);
		return new;
	}
}

struct listEntry *get_listElemPtr_mutex(){
	struct listEntry *finder = LIST_FIRST(&listhead);
	return finder;
}

struct listEntry *get_listElemPtr_semaphore(int sem_kind){
	struct listEntry *finder = LIST_FIRST(&listhead);
	while((sem_kind != finder->kind) && (finder != NULL)){
		finder = finder->listEntries.le_next;
	}
	return finder;
}

void remove_list(struct listEntry *tar){
	struct listEntry *finder = tar;
	LIST_REMOVE(finder, listEntries);
	free(finder);
}

void clean_all_list(){
    struct listEntry *ptr;
    while(!LIST_EMPTY(&listhead)){
        ptr = LIST_FIRST(&listhead);
        LIST_REMOVE(ptr, listEntries);
        free(ptr);
    }
}


/******************************************************************
 * Spinlock implementation
 */
void init_spinlock(struct spinlock *lock)
{
	lock->integer = (volatile int)0;
	return;
}

void acquire_spinlock(struct spinlock *lock)
{
	while(compare_and_swap(&(lock->integer), 0, 1) != 0);
	return;
}

void release_spinlock(struct spinlock *lock)
{	
	compare_and_swap(&(lock->integer), lock->integer, 0);
	return;
}


/******************************************************************
 * Variables for Mutex & Semaphore
 */
struct spinlock sl_list;	/* spinlock variable for list-safety */


/******************************************************************
 * Signal Handler for pthread_kill().
 */
void signal_handler(int sig){
	return;
}


/******************************************************************
 * Blocking lock implementation
 */
void init_mutex(struct mutex *lock)
{
	lock->integer = (volatile int)1;
	init_spinlock(&sl_list);
	init_list();
	return;
}

void acquire_mutex(struct mutex *lock)
{
	acquire_spinlock(&sl_list);
	if (!compare_and_swap(&(lock->integer), 1, 0)){
		struct listEntry *tar = insert_list(pthread_self());
		release_spinlock(&sl_list);
		signal(SIGUSR1, signal_handler);
		pause();
		tar->chkrcv = (volatile int)1;
	}
	else{
		release_spinlock(&sl_list);
	}
	return;
}

void release_mutex(struct mutex *lock)
{
	acquire_spinlock(&sl_list);
	if (LIST_EMPTY(&listhead)){
		lock->integer = (volatile int)1;
	}
	else{
		struct listEntry *tar = get_listElemPtr_mutex();
		pthread_t ret = tar->tid;
		do {
			usleep(30);
			pthread_kill(ret, SIGUSR1);
		} while (tar->chkrcv == 0);
		remove_list(tar);
	}
	release_spinlock(&sl_list);
	return;
}


/******************************************************************
 * Semaphore implementation
 */
void init_semaphore(struct semaphore *sem, int S)
{	
	sem->integer = (volatile int)(S);
	switch (S){
		case 0:			/* semaphore_full */
			sem->kind = (volatile int)0;
			break;
		case 1:			/* semaphore_mutex */
			sem->kind = (volatile int)1;
			break;
		default:		/* semaphore_empty */
			sem->kind = (volatile int)2;
			break;
	}
	init_spinlock(&sl_list);
	init_list();
	return;
}

void wait_semaphore(struct semaphore *sem)
{
	acquire_spinlock(&sl_list);
	sem->integer--;

	if (sem->integer < 0){
		struct listEntry *tar = insert_list(pthread_self());
		tar->kind = sem->kind;

		signal(SIGUSR1, signal_handler);
		release_spinlock(&sl_list);
		pause();
		tar->chkrcv = (volatile int)1;
	}
	else{
		assert(sl_list.integer == 1);
		release_spinlock(&sl_list);
	}
	return;
}

void signal_semaphore(struct semaphore *sem)
{
	acquire_spinlock(&sl_list);
	sem->integer++;
	if (sem->integer <= 0){
		struct listEntry *tar = get_listElemPtr_semaphore(sem->kind);
		pthread_t ret = tar->tid;
		while (tar->chkrcv == 0){
			pthread_kill(ret, SIGUSR1);
			usleep(10);
		}
		remove_list(tar);
	}
	release_spinlock(&sl_list);
	return;
}

/******************************************************************
 * Spinlock tester exmaple
 */
struct spinlock testlock;
int testlock_held = 0;

void *test_thread(void *_arg_)
{
	usleep(random() % 1000 * 1000);

	printf("Tester acquiring the lock...\n");
	acquire_spinlock(&testlock);
	printf("Tester acquired\n");
	assert(testlock_held == 0);
	testlock_held = 1;

	sleep(1);

	printf("Tester releases the lock\n");
	testlock_held = 0;
	release_spinlock(&testlock);
	printf("Tester released the lock\n");
	return 0;
}

void test_lock(void)
{
	/* Set nr_testers as you need
	 *  1: one main, one tester. easy :-)
	 * 16: one main, 16 testers contending the lock :-$
	 */
	const int nr_testers = 16;
	int i;
	pthread_t tester[nr_testers];

	printf("Main initializes the lock\n");
	init_spinlock(&testlock);

	printf("Main graps the lock...");
	acquire_spinlock(&testlock);
	assert(testlock_held == 0);
	testlock_held = 1;
	printf("acquired!\n");

	for (i = 0; i < nr_testers; i++) {
		pthread_create(tester + i, NULL, test_thread, NULL);
	}

	sleep(1);

	printf("Main releases the lock\n");
	testlock_held = 0;
	release_spinlock(&testlock);
	printf("Main released the lock\n");

	for (i = 0; i < nr_testers; i++) {
		pthread_join(tester[i], NULL);
	}
	assert(testlock_held == 0);
	printf("Your spinlock implementation looks O.K.\n");

	return;
}

