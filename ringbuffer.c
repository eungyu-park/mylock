#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <assert.h>
#include <unistd.h>
#include <sys/queue.h>
#include <string.h>

#include "config.h"
#include "locks.h"

static int nr_slots = 0;

static enum lock_types lock_type;

void (*enqueue_fn)(int value) = NULL;
int (*dequeue_fn)(void) = NULL;

void enqueue_ringbuffer(int value)
{
	assert(enqueue_fn);
	assert(value >= MIN_VALUE && value < MAX_VALUE);

	enqueue_fn(value);
}

int dequeue_ringbuffer(void)
{
	int value;

	assert(dequeue_fn);

	value = dequeue_fn();
	assert(value >= MIN_VALUE && value < MAX_VALUE);

	return value;
}


/******************************************************************
 * Definition of spinlock, mutex, and semaphores.
 */
struct spinlock sl;

struct mutex mu;

struct semaphore sem_empty;
struct semaphore sem_mutex;
struct semaphore sem_full;


/*********************************************************************
 * My Circular Queue implementation.
 * Queue-related tasks.
 * 
 * Definition, Initialization, Insert, Delete(pop), Clean(free),
 * Checking Number of Elements in Queue(for prevent overflow),
 * Traversal(for debugging).
 */
CIRCLEQ_HEAD(circleq, entry) head;
struct circleq *headp;
struct entry {
	int value;
	CIRCLEQ_ENTRY(entry) entries;
};

void init_circleq(){
	CIRCLEQ_INIT(&head);
}

void insert_circleq(int value){
	struct entry *new = (struct entry *)malloc(sizeof(struct entry));
	new->value = value;
	CIRCLEQ_INSERT_TAIL(&head, new, entries);
}

int delete_circleq(){
    struct entry *target = CIRCLEQ_FIRST(&head);
    int tmpVal = target->value;
    CIRCLEQ_REMOVE(&head, target, entries);
    free(target);
    return tmpVal;
}

int chkSlots_circleq(){
	int count = 0;
	struct entry *ptr = head.cqh_first;
	if (ptr == NULL){
		return 0;
	}
	else{
		for (; ptr != (void *)&head; ptr = ptr->entries.cqe_next){
			count++;
		}
	}
	return count;
}

void clean_all_circleq(){
    struct entry *ptr;
    while((ptr = CIRCLEQ_FIRST(&head)) != (void *)&head){
        CIRCLEQ_REMOVE(&head, ptr, entries);
        free(ptr);
    }
}

void traversal_circleq(){
	write(STDOUT_FILENO, "Queue:\t", sizeof("Queue:\t"));
    struct entry *ptr;
	for (ptr = head.cqh_first; ptr != (void *)&head; ptr = ptr->entries.cqe_next){
        if (ptr == NULL){
			continue;
		}
		char buf[64];
		sprintf(buf, "%d ", ptr->value);
		write(STDOUT_FILENO, buf, strlen(buf));
    }
	write(STDOUT_FILENO, "\n", sizeof("\n"));
}


/*********************************************************************
 * TODO: Implement using spinlock
 */
void enqueue_using_spinlock(int value)
{	
sl_chkQ_en:
	acquire_spinlock(&sl);
	while(chkSlots_circleq() >= nr_slots){
		release_spinlock(&sl);
		goto sl_chkQ_en;
	}
	insert_circleq(value);

	/* 현재의 원형 큐 상태를 보려면, 아래의 함수 traversal_circleq()를 사용하시면 됩니다. */
	/* 디버깅용으로 사용하였으나, 채점 시 편의를 위해 남겨놓습니다. */
	//traversal_circleq();

	release_spinlock(&sl);
}

int dequeue_using_spinlock(void)
{
sl_chkQ_de:
	acquire_spinlock(&sl);
	while(CIRCLEQ_EMPTY(&head)){
		release_spinlock(&sl);
		goto sl_chkQ_de;
	}
	int rv =  delete_circleq();
	release_spinlock(&sl);
	return rv;
}

void init_using_spinlock(void)
{
	init_spinlock(&sl);
	init_circleq();
	enqueue_fn = &enqueue_using_spinlock;
	dequeue_fn = &dequeue_using_spinlock;
}

void fini_using_spinlock(void)
{
	clean_all_circleq();
}


/*********************************************************************
 * TODO: Implement using mutex
 */
void enqueue_using_mutex(int value)
{
mu_chkQ_en:
	acquire_mutex(&mu);
	while(chkSlots_circleq() >= nr_slots){
		release_mutex(&mu);
		goto mu_chkQ_en;
	}
	insert_circleq(value);

	/* 현재의 원형 큐 상태를 보려면, 아래의 함수 traversal_circleq()를 사용하시면 됩니다. */
	/* 디버깅용으로 사용하였으나, 채점 시 편의를 위해 남겨놓습니다. */
	//traversal_circleq();

	release_mutex(&mu);
}
int dequeue_using_mutex(void)
{
mu_chkQ_de:
	acquire_mutex(&mu);
	while(CIRCLEQ_EMPTY(&head)){
		release_mutex(&mu);
		goto mu_chkQ_de;
	}
	int rv = delete_circleq();
	release_mutex(&mu);
	return rv;
}

void init_using_mutex(void)
{
	init_mutex(&mu);
	init_circleq();
	enqueue_fn = &enqueue_using_mutex;
	dequeue_fn = &dequeue_using_mutex;
}

void fini_using_mutex(void)
{
	clean_all_circleq();
	clean_all_list();
}


/*********************************************************************
 * TODO: Implement using semaphore
 */
void enqueue_using_semaphore(int value)
{
	wait_semaphore(&sem_empty);
	wait_semaphore(&sem_mutex);
	insert_circleq(value);

	/* 현재의 원형 큐 상태를 보려면, 아래의 함수 traversal_circleq()를 사용하시면 됩니다. */
	/* 디버깅용으로 사용하였으나, 채점 시 편의를 위해 남겨놓습니다. */
	//traversal_circleq();

	signal_semaphore(&sem_mutex);
	signal_semaphore(&sem_full);
}

int dequeue_using_semaphore(void)
{
	wait_semaphore(&sem_full);
	wait_semaphore(&sem_mutex);
	int rv = delete_circleq();
	signal_semaphore(&sem_mutex);
	signal_semaphore(&sem_empty);
	return rv;
}

void init_using_semaphore(void)
{
	init_semaphore(&sem_empty, nr_slots);
	init_semaphore(&sem_mutex, 1);
	init_semaphore(&sem_full, 0);
	init_circleq();
	enqueue_fn = &enqueue_using_semaphore;
	dequeue_fn = &dequeue_using_semaphore;
}

void fini_using_semaphore(void)
{
	clean_all_circleq();
	clean_all_list();
}


/*********************************************************************
 * Common implementation
 */
int init_ringbuffer(const int _nr_slots_, const enum lock_types _lock_type_)
{
	assert(_nr_slots_ > 0);
	nr_slots = _nr_slots_;

	/* Initialize lock! */
	lock_type = _lock_type_;
	switch (lock_type) {
	case lock_spinlock:
		init_using_spinlock();
		break;
	case lock_mutex:
		init_using_mutex();
		break;
	case lock_semaphore:
		init_using_semaphore();
		break;
	}

	/* TODO: Initialize your ringbuffer and synchronization mechanism */
	// Nothing to do.

	return 0;
}

void fini_ringbuffer(void)
{
	/* TODO: Clean up what you allocated */
	switch (lock_type) {
	case lock_spinlock:
		fini_using_spinlock();
		break;
	case lock_mutex:
		fini_using_mutex();
		break;
	case lock_semaphore:
		fini_using_semaphore();
	default:
		break;
	}
}
