#ifndef _ATOMIC_H_
#define _ATOMIC_H_

/**
 * Set *@value to @new if *@value was @old.
 * Return the old value of *@value
 *
 * *value  old  new  -->  return *value
 *      0   0    0             0   0
 *      0   0    1             0   1
 *      0   1    0             0   0
 *
 *      1   1    1             1   1
 *      1   1    0             1   0
 *      1   0    0             1   1
 */

// value와 old가 동일하다면, value를 new로 바꿈.
// value와 old가 동일하지 않다면, 현상유지.
// 항상 예전 value를 반환.
static inline int compare_and_swap(volatile int *value, int old, int new)
{
	asm volatile (
		"lock ; cmpxchg %3, %1"
			: "=a"(old), "=m"(*value)
			: "a"(old), "r"(new)
			: "memory" );
	return old;
}
#endif
